import csv

import pandas as pd


class Date:
    def __init__(self, jour, mois, annee):
        self.mois = mois
        self.annee = annee
        self.jour = jour

    def __eq__(self, date):
        if date.mois == self.mois and date.jour == self.jour and date.annee == self.annee:
            return True
        else:
            return False

    def __it__(self, date):
        if self.annee < date.annee:
            return True
        elif self.annee > date.annee:
            return False
        else:
            if self.mois < date.mois:
                return True
            elif self.mois > date.mois:
                return False
            else:
                if self.jour < date.jour:
                    return True
                else:
                    return False


class Etudiant:
    def __init__(self, nom, prenom, dateNaissance):
        self.nom = nom
        self.prenom = prenom
        self.dateNaissance = dateNaissance
        self.email = ""

    def adresselec(self):
        self.email = self.prenom + "." + self.nom + "@etu.univ-tours.fr"
        return self.email

    def age(self):
        date = self.dateNaissance.split("/")

        datetime = Date(23, 9, 2022)
        return datetime.annee - int(date[2])


def listeEtudiantsExcel():
    listeEtu = []
    with open("fichetu.csv", "r") as csvFichier:
        fichier = csv.reader(csvFichier)

        for lignes in fichier:
            tupleEtu = tuple(lignes[0].split(";"))
            etudiant = Etudiant(tupleEtu[0], tupleEtu[1], tupleEtu[2])
            listeEtu.append(etudiant)
    return listeEtu


def main():
    print("Exercice 3 :\n")
    liste = listeEtudiantsExcel()
    for i in liste:
        print(i.nom + "\t" + i.prenom + "\t" + i.adresselec() + "\t" + str(i.age()) + "\n")


if __name__ == "__main__":
    main()
