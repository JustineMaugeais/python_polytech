import sys

exceptionFichierNonRenseigne = "Le nom du fichier n'a pas été renseigné.\n"
exceptionMauvaiseSaisie = "Vous devez saisir 1, 2, 3, 4 ou 9...\n"


def choisirNomFichier():
    nomFichier = input("Entrer le nom du fichier :\n")
    print("Nom du fichier saisi : " + nomFichier)
    fichier = open(nomFichier, "a")
    fichier.close()
    return nomFichier


def ajouterTexteFichier(nomFichier):
    try:
        texte = input("Entrez le texte du fichier :\n")
        with open(nomFichier, "r+") as fichier:
            fichier.write(texte)
    except AttributeError:
        print(exceptionFichierNonRenseigne)


def afficherFichierComplet(nomFichier):
    try:
        with open(nomFichier, "r") as fichier:
            print("Affichage du fichier complet :\n" + fichier.read())
    except AttributeError:
        print(exceptionFichierNonRenseigne)


def viderFichier(nomFichier):
    try:
        with open(nomFichier, "r+") as fichier:
            fichier.truncate(0)
    except AttributeError:
        print(exceptionFichierNonRenseigne)


def quitterProgramme():
    sys.exit()


def printMenu():
    print(
        "1. Choisir un nom de fichier\n"
        "2. Ajouter un texte\n"
        "3. Afficher le fichier complet\n"
        "4. Vider le fichier\n"
        "9. Quitter le programme\n")


def printSeparation():
    print("________________________________________________________________________________________________\n")


def main():
    printSeparation()
    nomFichier = ''
    while True:
        printMenu()
        valeur = 0
        try:
            valeur = int(input("Saisir une de ces valeurs :\n"))
        except AttributeError:
            int(input(exceptionMauvaiseSaisie))
        if valeur == 1:
            nomFichier = choisirNomFichier()
        elif valeur == 2:
            ajouterTexteFichier(nomFichier)
        elif valeur == 3:
            afficherFichierComplet(nomFichier)
        elif valeur == 4:
            viderFichier(nomFichier)
        elif valeur == 9:
            quitterProgramme()
        printSeparation()


if __name__ == "__main__":
    main()
