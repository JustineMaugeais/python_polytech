import math
from tkinter import *

formule = ""
memoire = ""
modeBasique = True
modeBasiqueAvant = False


def cliquerSurBouton(num):
    """Récupère la valeur du bouton, l'ajoute à la formule et met à jour l'équation."""
    global formule
    formule = formule + str(num)
    equation.set(formule)


def cliquerSurEgal():
    """Récupère la dernière formule saisie, calcule le résultat et met à jour l'équation et la formule avec le nouveau résultat.
    \nEn cas d'erreur, une exception est levée et un message appaaraît sur la calculatrice et la formule est vidée."""
    try:
        global formule
        if formule.__contains__("cos"):
            formule.replace("cos", "math.cos")
        print(formule)

        result = str(eval(formule))
        equation.set(result)
        formule = result
    except ArithmeticError:
        equation.set("Erreur !")
        formule = ""


def cliquerSurAC():
    """Vide le contenu total en effacant celui de la formule et l'équation"""
    global formule
    formule = ""
    equation.set("")


def cliquerSurC():
    """Efface le dernier élément cliqué."""
    global formule
    formule = formule[:-1]
    equation.set(formule)


def cliquerSurMemoire():
    global formule
    global memoire

    if formule == "":
        formule = memoire
        equation.set(formule)
    else:
        memoire = formule


def afficherCalculatriceBasique():
    formule_field = Entry(calculatrice, textvariable=equation)
    formule_field.grid(columnspan=4, pady=30, padx=20, ipadx=100, ipady=10)
    bouton1 = Button(calculatrice, text=' 1 ', command=lambda: cliquerSurBouton(1), height=2, width=10)
    bouton1.grid(row=2, column=0)

    bouton2 = Button(calculatrice, text=' 2 ', command=lambda: cliquerSurBouton(2), height=2, width=10)
    bouton2.grid(row=2, column=1)

    bouton3 = Button(calculatrice, text=' 3 ', command=lambda: cliquerSurBouton(3), height=2, width=10)
    bouton3.grid(row=2, column=2)

    bouton4 = Button(calculatrice, text=' 4 ', command=lambda: cliquerSurBouton(4), height=2, width=10)
    bouton4.grid(row=3, column=0)

    bouton5 = Button(calculatrice, text=' 5 ', command=lambda: cliquerSurBouton(5), height=2, width=10)
    bouton5.grid(row=3, column=1)

    bouton6 = Button(calculatrice, text=' 6 ', command=lambda: cliquerSurBouton(6), height=2, width=10)
    bouton6.grid(row=3, column=2)

    bouton7 = Button(calculatrice, text=' 7 ', command=lambda: cliquerSurBouton(7), height=2, width=10)
    bouton7.grid(row=4, column=0)

    bouton8 = Button(calculatrice, text=' 8 ', command=lambda: cliquerSurBouton(8), height=2, width=10)
    bouton8.grid(row=4, column=1)

    bouton9 = Button(calculatrice, text=' 9 ', command=lambda: cliquerSurBouton(9), height=2, width=10)
    bouton9.grid(row=4, column=2)

    bouton0 = Button(calculatrice, text=' 0 ', command=lambda: cliquerSurBouton(0), height=2, width=10)
    bouton0.grid(row=5, column=0)

    boutonPlus = Button(calculatrice, text=' + ', command=lambda: cliquerSurBouton("+"), height=2, width=10)
    boutonPlus.grid(row=2, column=3)

    boutonMoins = Button(calculatrice, text=' - ', command=lambda: cliquerSurBouton("-"), height=2, width=10)
    boutonMoins.grid(row=3, column=3)

    boutonMultiplication = Button(calculatrice, text=' * ', command=lambda: cliquerSurBouton("*"), height=2, width=10)
    boutonMultiplication.grid(row=4, column=3)

    boutonDivision = Button(calculatrice, text=' / ', command=lambda: cliquerSurBouton("/"), height=2, width=10)
    boutonDivision.grid(row=5, column=3)

    boutonEgal = Button(calculatrice, text=' = ', command=cliquerSurEgal, height=2, width=10)
    boutonEgal.grid(row=5, column=2)

    boutonAC = Button(calculatrice, text='AC', command=cliquerSurAC, height=2, width=10)
    boutonAC.grid(row=6, column='0')

    boutonDecimal = Button(calculatrice, text='.', command=lambda: cliquerSurBouton('.'), height=2, width=10)
    boutonDecimal.grid(row=5, column=1)

    boutonPourcent = Button(calculatrice, text='%', command=lambda: cliquerSurBouton('%'), height=2, width=10)
    boutonPourcent.grid(row=6, column=1)

    boutonC = Button(calculatrice, text='C', command=cliquerSurC, height=2, width=10)
    boutonC.grid(row=6, column=2)

    boutonMemoire = Button(calculatrice, text='M', command=cliquerSurMemoire, height=2, width=10)
    boutonMemoire.grid(row=6, column=3)

    boutonParentheseGauche = Button(calculatrice, text='(', command=lambda: cliquerSurBouton("("), height=2, width=10)
    boutonParentheseGauche.grid(row=2, column=4)

    boutonParentheseDroite = Button(calculatrice, text=')', command=lambda: cliquerSurBouton(")"), height=2, width=10)
    boutonParentheseDroite.grid(row=3, column=4)


def afficherCalculatriceScientifique():
    """Permet d'adapter la taille de la fenêtre et de modifier l'affichage de certains boutons selon le mode
    utilisé. """
    global calculatrice

    if not modeBasique:
        calculatrice.geometry("445x375")
        boutonCosinus.grid(row=7, column=0)
        boutonSinus.grid(row=7, column=1)
        boutonTan.grid(row=7, column=2)
        boutonPuissance.grid(row=7, column=3)

    elif modeBasique:
        calculatrice.geometry("445x315")
        boutonCosinus.grid_forget()
        boutonSinus.grid_forget()
        boutonTan.grid_forget()
        boutonPuissance.grid_forget()


def cosinus():
    """Fonction pour calculer le cosinus"""
    global formule
    try:
        cos = math.cos(float(formule))
        formule = str(cos)
        equation.set(formule)
    except ArithmeticError:
        equation.set("Erreur !")
        formule = ""


def sinus():
    global formule
    try:
        sin = math.sin(float(formule))
        formule = str(sin)
        equation.set(formule)
    except ArithmeticError:
        equation.set("Erreur !")
        formule = ""


def tan():
    global formule
    try:
        tang = math.tan(float(formule))
        formule = str(tang)
        equation.set(formule)
    except ArithmeticError:
        equation.set("Erreur !")
        formule = ""


def menuAide():
    """Affichage de la fenêtre d'aide."""
    fenetreAide = Toplevel(calculatrice)
    fenetreAide.geometry("750x250")
    fenetreAide.title("Aide")
    Label(fenetreAide, text="Pour pouvoir basculer d'un mode à l'autre, il suffit d'aller dans le menu et cliquer sur "
                            ":\nMode basique/scientifique.").place(x=150, y=80)


def menuMode():
    """Sélection du mode de la calculatrice et affichage mis à jour selon le mode choisi."""
    global modeBasique
    if modeBasique:
        modeBasique = False
    else:
        modeBasique = True

    afficherCalculatriceScientifique()


def constructionMenu():
    global calculatrice
    menubar = Menu(calculatrice)
    calculatrice.config(menu=menubar)
    file_menu = Menu(menubar)

    file_menu.add_command(label='Aide', command=menuAide)
    file_menu.add_command(label='Mode basique/scientifique', command=menuMode)
    file_menu.add_command(label='Quitter', command=calculatrice.destroy)

    # add the File menu to the menubar
    menubar.add_cascade(label="Menu", menu=file_menu)


if __name__ == "__main__":
    calculatrice = Tk()

    calculatrice.title("Calculatrice")
    calculatrice.geometry("445x315")

    constructionMenu()

    equation = StringVar()
    afficherCalculatriceBasique()

    # Boutons pour la calculatrice scientifique
    boutonCosinus = Button(calculatrice, text='cos', command=cosinus, height=2, width=10)
    boutonSinus = Button(calculatrice, text='sin', command=sinus, height=2, width=10)
    boutonTan = Button(calculatrice, text='tan', command=tan, height=2, width=10)
    boutonPuissance = Button(calculatrice, text='^', command=lambda: cliquerSurBouton("**"), height=2, width=10)

    calculatrice.mainloop()
