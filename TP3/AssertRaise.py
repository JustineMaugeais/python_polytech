def assertionKO():
    """Renvoie une exception car assert(1 == 0) est faux."""
    assert (1 == 0)


def assertionOK():
    """Passe car assert(1 == 1) est vrai."""
    assert (1 == 1)


def raiseException():
    raise Exception("Erreur : 0 ne peut pas être égal à 1 !\n")


def main():
    try:
        print("Assertion sans erreur :\n")
        assertionOK()
        print("Assertion passée\n")
        print("Assertion avec erreur :\n")
        assertionKO()

        # Ne devrait jamais s'afficher car exception levée avant.
        print("Assertion passée\n")
    except AssertionError as error:
        print(error)
        print("1 ne peux pas être égal à 0 !")

    print("Le programme continue.\nOn utilise raise pour lever une exception.")
    raiseException()
    print("Le programme s'est arrêté. On ne verra jamais ce message.")


if __name__ == "__main__":
    main()
