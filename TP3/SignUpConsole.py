import csv
import hashlib
import sys

import bcrypt as bcrypt
import maskpass
from Crypto.Cipher import AES, PKCS1_OAEP

# TODO : password en global ???
exceptionFichierNonRenseigne = "Le nom du fichier n'a pas été renseigné.\n"
exceptionMauvaiseSaisie = "Vous devez saisir 1 ou 2...\n"

"""La fonction gensalt génère un sel aléatoire. Il est appelé une fois, et seulement ici afin de pouvoir garder et comparer en fonction de celui-ci. 
Donc le programme général ne fonction que lorsque l'application lancée est toujours la même. Car à chaque lancement,
le sel change et donc les login prédemment encodé ne peuvent plus être retrouvés."""
sel = bcrypt.gensalt()
isLogged = False


def choisirNomFichier():
    nomFichier = input("Entrer le nom du fichier :\n")
    print("Nom du fichier saisi : " + nomFichier)
    fichier = open(nomFichier, "a")
    fichier.close()
    return nomFichier


def viderFichier(nomFichier):
    try:
        with open(nomFichier, "r+") as fichier:
            fichier.truncate(0)
    except AttributeError:
        print(exceptionFichierNonRenseigne)


def quitterProgramme():
    sys.exit()


def printMenu():
    if isLogged is False:
        print(
            "1. S\'enregistrer\n"
            "2. Se connecter\n"
            "3. Quitter le programme\n")
    else:
        print(
            "1. Se déconnecter\n"
            "2. Crypter un fichier\n"
            "3. Décrypter un fichier"
            "4. Quitter le programme\n")


def printSeparation():
    print("________________________________________________________________________________________________\n")


def valeurLogin():
    return str(input("Login : "))


def valeurPwd():
    return maskpass.askpass(prompt="Password : ", mask="*")


def signUpNow(login, password):
    if login == "" or password == "":
        print("Recommencer !")
    else:
        print("Coucou " + login + " :)")

        # Ecriture du login et du mot de passe dans le fichier
        with open("signUps.csv", "a") as fichier:
            fichier.write(bcrypt.hashpw(login.encode(), sel).decode() + ";"
                          + hashlib.sha512(password.encode()).hexdigest() + "\n")

        print("Tu as bien été inscrit ! Tu peux désormais te connecter avec tes identifiants ;)\n")


def signIn():
    global isLogged
    login = str(input("Login :\n"))
    password = valeurPwd()
    print("LOGIN = " + login + "\n")

    members = []
    with open("signUps.csv", "r") as fichier:
        lignes = csv.reader(fichier)
        for ligne in lignes:
            tupleMember = tuple(ligne[0].split(";"))
            loginMember = tupleMember[0]
            pwdMember = tupleMember[1]
            members.append([loginMember, pwdMember])

    memberPwd = hashlib.sha512(password.encode()).hexdigest()
    memberName = bcrypt.hashpw(login.encode(), sel).decode()

    for member in members:
        if member[1] == memberPwd and member[0] == memberName:
            isLogged = True
            print("Ca y est vous êtes bien connecté !")
        else:
            print("Désolée, le mot de passse ou l'utilisateur ne correspond pas !")


def logOut():
    global isLogged
    isLogged = False
    print("Vous êtes bien déconnecté :)")


def crypterFichier(nom, password):
    with open("fichierCrypteToto.txt", "wb") as fichier:
        fichierAEncoder = open(nom, "r").read()
        cipher = AES.new(password, AES.MODE_EAX)
        ciphertext, tag = cipher.encrypt_and_digest(fichierAEncoder.encode())
        [fichier.write(x) for x in (b'', cipher.nonce, tag, ciphertext)]


def decrypterFichier(nom, password):
    # TODO : Terminer decryptage fichier RELOU
    with open(nom, "rb") as fichier:
        ciphertext = fichier.read()
        cipher = PKCS1_OAEP.new(password)
        session_key = cipher.decrypt(b'')

        # Decrypt the data with the AES session key
        cipher_aes = AES.new(session_key, AES.MODE_EAX, b'')
        text = cipher_aes.decrypt_and_verify(ciphertext, ciphertext)
        print(text.decode())


def main():
    printSeparation()
    login = ''
    password = ''
    while True:
        printMenu()
        valeur = 0
        try:
            valeur = int(input("Saisir une de ces valeurs :\n"))
        except AttributeError:
            int(input(exceptionMauvaiseSaisie))
        if isLogged is False:
            if valeur == 1:
                login = valeurLogin()
                password = valeurPwd()

                signUpNow(login, password)
            elif valeur == 2:
                signIn()
            elif valeur == 3:
                quitterProgramme()
            printSeparation()
        else:
            passwordHash = hashlib.md5(password.encode()).digest()
            if valeur == 1:
                logOut()
            elif valeur == 2:
                nomF = choisirNomFichier()
                crypterFichier(nomF, passwordHash)
            elif valeur == 3:
                nomF = choisirNomFichier()
                decrypterFichier(nomF, passwordHash)
            elif valeur == 4:
                quitterProgramme()


if __name__ == "__main__":
    main()
