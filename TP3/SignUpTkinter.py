from tkinter import *


def signUpNow(lgn, pwd):
    global login
    if login.get() == "" or pwd.get() == "":
        print("Erreur")
    else:
        print(login.get())
    login: object = login.set("")
    pwd = pwd.set("")


def afficherSaisieLogin(fenetre):
    labelLogin = Label(fenetre, text="Login : ").grid(row=2, column=1)
    entryLogin = Entry(fenetre, textvariable=login, ).grid(row=2, column=2)


def afficherSaisiePassword(fenetre):
    global password
    labelPassword = Label(fenetre, text="Password : ").grid(row=1, column=1)

    entryPassword = Entry(fenetre, textvariable=password, show='*').grid(row=1, column=2)


def afficherBoutonSignUp(fenetre):
    loginButton = Button(fenetre, text="Sign up", command=lambda: signUpNow).grid(row=4, column=2, columnspan=3)


def main():
    global login
    login = StringVar
    password = StringVar

    signUp = Tk()
    signUp.title("Enregistrement")
    signUp.geometry("445x315")
    Label(signUp, text="Password : ").grid(row=1, column=1)
    Entry(signUp, textvariable=password, show='*').grid(row=1, column=2)

    Label(signUp, text="Login : ").grid(row=2, column=1)
    Entry(signUp, textvariable=login, ).grid(row=2, column=2)

    Button(signUp, command=lambda: signUpNow(login, password), text="Sign up").grid(row=4, column=2, columnspan=3)

    signUp.mainloop()


if __name__ == "__main__":
    main()
