from numpy.random import randint


def genererNombreAleatoire(a, b):
    """Genere un nombre entre a et b inclus."""
    return randint(a, b + 1)


def main():
    print(genererNombreAleatoire(1, 4))


if __name__ == "__main__":
    main()
