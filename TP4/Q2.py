import matplotlib.pyplot as plt
from numpy.random import randint


def genererNombreAleatoire(a, b):
    """Genere un nombre entre a et b inclus."""
    return randint(a, b + 1)


def genererListeNombresAleatoires(a, b, c):
    listeNbAlea = []
    for i in range(c):
        listeNbAlea.append(genererNombreAleatoire(a, b))
    return listeNbAlea


def afficherPointsListe(liste):
    plt.title("Q2")
    plt.plot(liste, 'r.')
    plt.show()


def main():
    listeNbAlea = genererListeNombresAleatoires(1, 100, 200)
    for nb in listeNbAlea:
        print(nb)
    afficherPointsListe(listeNbAlea)


if __name__ == "__main__":
    main()
