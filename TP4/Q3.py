import matplotlib.pyplot as plt
from numpy.random import randint


def genererNombreAleatoire(a, b):
    """Genere un nombre entre a et b inclus."""
    return randint(a, b + 1)


def genererListeNombresAleatoires(a, b, c):
    """Generation d'une liste de c nombres aleatoire, compris entre a et b."""
    listeNbAlea = []
    for i in range(c):
        listeNbAlea.append(genererNombreAleatoire(a, b))
    return listeNbAlea


def afficherPointsListe(liste):
    """Affiche le graphe correspondant a la liste."""
    plt.title("Q2")
    plt.plot(liste, 'r.')
    plt.show()


def afficherPointsListeAvecStyleGrapheSepares(nbRow, nbcol, pos, titre, liste, couleur, epaisseur, marqueur):
    """Permet le placement du graphe dans la fenetre avec les autres et sa personnalisation."""
    plt.subplot(nbRow, nbcol, pos)
    plt.plot(liste, couleur, linewidth=epaisseur, marker=marqueur)
    plt.title(titre)


def afficherGraphesSepares():
    """Affiche un graphe pour chaque courbe dans la meme fenetre."""
    listeNbAlea1 = genererListeNombresAleatoires(1, 100, 100)
    listeNbAlea2 = genererListeNombresAleatoires(1, 50, 100)
    listeNbAlea3 = genererListeNombresAleatoires(20, 80, 100)

    afficherPointsListeAvecStyleGrapheSepares(3, 1, 1, "G1", listeNbAlea1, "b", 0.25, ".")
    afficherPointsListeAvecStyleGrapheSepares(3, 1, 2, "G2", listeNbAlea2, "g", 1.0, "+")
    afficherPointsListeAvecStyleGrapheSepares(3, 1, 3, "G3", listeNbAlea3, "r", 0.5, "*")

    plt.show()


def main():
    afficherGraphesSepares()


if __name__ == "__main__":
    main()
