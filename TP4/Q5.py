from random import random

import matplotlib.pyplot as plt
from numpy.random import randint


def genererNombreAleatoire(a, b):
    """Genere un nombre entre a et b inclus."""
    return randint(a, b + 1)


def genererListeNombresAleatoires(a, b, c):
    """Generation d'une liste de c nombres aleatoire, compris entre a et b."""
    listeNbAlea = []
    for i in range(c):
        listeNbAlea.append(genererNombreAleatoire(a, b))
    return listeNbAlea


def afficherPointsListe(liste):
    """Affiche le graphe correspondant a la liste."""
    plt.title("Q2")
    plt.plot(liste, 'r.')
    plt.show()


def afficherPointsListeAvecStyleGrapheSepares(nbRow, nbcol, pos, titre, liste, couleur, epaisseur, marqueur):
    """Permet le placement du graphe dans la fenetre avec les autres et sa personnalisation."""
    plt.subplot(nbRow, nbcol, pos)
    plt.plot(liste, couleur, linewidth=epaisseur, marker=marqueur)
    plt.title(titre)


def afficherPointsListeAvecStyleGrapheEnsemble(label, liste, couleur, epaisseur, marqueur):
    """Permet de definir une legende pour la courbe dans le graphe et sa personnalisation."""
    plt.plot(liste, couleur, linewidth=epaisseur, marker=marqueur, label=label)
    plt.legend()


def afficherGraphesSepares():
    """Affiche un graphe pour chaque courbe dans la meme fenetre."""
    listeNbAlea1 = genererListeNombresAleatoires(1, 100, 100)
    listeNbAlea2 = genererListeNombresAleatoires(1, 50, 100)
    listeNbAlea3 = genererListeNombresAleatoires(20, 80, 100)

    afficherPointsListeAvecStyleGrapheSepares(3, 1, 1, "G1", listeNbAlea1, "b", 0.25, ".")
    afficherPointsListeAvecStyleGrapheSepares(3, 1, 2, "G2", listeNbAlea2, "g", 1.0, "+")
    afficherPointsListeAvecStyleGrapheSepares(3, 1, 3, "G3", listeNbAlea3, "r", 0.5, "*")

    plt.show()


def afficherGraphesEnsemble():
    """Affiche trois courbe dans un meme graphe avec leur legende."""
    listeNbAlea1 = genererListeNombresAleatoires(1, 100, 50)
    listeNbAlea2 = genererListeNombresAleatoires(1, 50, 50)
    listeNbAlea3 = genererListeNombresAleatoires(20, 80, 50)

    afficherPointsListeAvecStyleGrapheEnsemble("G1", listeNbAlea1, "b", 0.25, ".")
    afficherPointsListeAvecStyleGrapheEnsemble("G2", listeNbAlea2, "g", 1.0, "+")
    afficherPointsListeAvecStyleGrapheEnsemble("G3", listeNbAlea3, "r", 0.5, "*")


def marquerLimiteSurGraphe(xy, xyText, text):
    plt.annotate(text, xy=xy, xytext=xyText,
                 arrowprops={'facecolor': 'black', 'shrink': 0.05})


def afficherHistogramme():
    # TODO : Changer valeur et PB UTF8
    # importing required libraries
    from matplotlib import pyplot as plt
    import numpy as np
    # A dataset of 10 students
    marks = np.array([98, 89, 45, 56, 78, 25, 43, 33, 54, 100])
    fig, axis = plt.subplots(figsize=(10, 5))
    axis.hist(marks, bins=[0, 20, 40, 60, 80, 100])
    # Displaying the graph
    plt.show()


def afficherCamember():
    # TODO : Changer valeurs et demande listes dans le main :)
    name = ['-18', '18-25', '25-50', '50+']
    data = [5000, 26000, 21400, 12000]

    explode = (0, 0.15, 0, 0)
    plt.pie(data, explode=explode, labels=name, autopct='%1.1f%%', startangle=90, shadow=True)
    plt.axis('equal')
    plt.show()


def main():
    #afficherCamember()
    afficherHistogramme()


if __name__ == "__main__":
    main()
