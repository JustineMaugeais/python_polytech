# coding=utf8

import csv
import sqlite3

from TP5.CommunesClasse import Commune
from TP5.DepartementsClasse import Departement
from TP5.RegionsClasse import Region

# Connexion à la base de données Pays
connection = sqlite3.connect("pays.db")

# Sera utile pour exécuter les requêtes
cursor = connection.cursor()

createTableCommunes = "CREATE TABLE Communes(codeCommune, nomCommune, populationCommune, codeDepartement);"
createTableDepartements = "CREATE TABLE Departements(codeDepartement, nomDepartement, codeRegion);"
createTableRegions = "CREATE TABLE Regions(codeRegion, nomRegion);"

deleteTableCommunes = "DROP TABLE Communes;"
deleteTableDepartements = "DROP TABLE Departements;"
deleteTableRegions = "DROP TABLE Regions;"

readTableCommunes = "SELECT * FROM Communes;"
readTableDepartements = "SELECT * FROM Departements;"
readTableRegions = "SELECT * FROM Regions;"


def listeRegionsExcel():
    """Retourne la liste des régions, exportées depuis un fichier CSV"""
    listeRegions = []
    with open("data/regions.csv", "r") as csvFichier:
        fichier = csv.reader(csvFichier)
        currentLine = 0
        for ligne in fichier:
            # On effectue la récupération des régions après l'entête de fichier
            if currentLine > 7:
                tupleRegion = tuple(ligne[0].split(";"))
                region = Region(tupleRegion[0], tupleRegion[1])
                # On ajoute la région à la liste seulement si elle n'est pas vide
                if (region.codeRegion != "") and (region.nomRegion != ""):
                    listeRegions.append(region)
            currentLine += 1
    return listeRegions


def listeDepartementsExcel():
    """Retourne la liste des départements, exportés depuis un fichier CSV"""
    listeDepartements = []
    with open("data/departements.csv", "r") as csvFichier:
        fichier = csv.reader(csvFichier)
        currentLine = 0
        for ligne in fichier:
            # On effectue la récupération des départements après l'entête de fichier
            if currentLine > 7:
                tupleDepartement = tuple(ligne[0].split(";"))
                departement = Departement(tupleDepartement[2], tupleDepartement[3], tupleDepartement[0])
                # On ajoute le département à la liste seulement si il n'est pas vide
                if (departement.codeDepartement != "") and (departement.nomDepartment != "") and (
                        departement.codeRegion != ""):
                    listeDepartements.append(departement)
            currentLine += 1
    return listeDepartements


def listeCommunesExcel():
    """Retourne la liste des communes, exportées depuis un fichier CSV"""
    listeCommunes = []
    with open("data/communes.csv", "r") as csvFichier:
        fichier = csv.reader(csvFichier)
        currentLine = 0
        for ligne in fichier:
            # On effectue la récupération des communes après l'entête de fichier
            if currentLine > 7:
                tupleCommune = tuple(ligne[0].split(";"))

                commune = Commune(tupleCommune[5], tupleCommune[6], tupleCommune[7], tupleCommune[2])
                # On ajoute la commune à la liste seulement si elle n'est pas vide
                if (commune.populationCommune != "") and (commune.codeCommune != "") and (
                        commune.codeDepartement != "") and (commune.nomCommune != ""):
                    listeCommunes.append(commune)
            currentLine += 1
    return listeCommunes


def ajoutCommunesDansBaseDeDonnees():
    """Ajout des communes dans la base de données."""
    listeCommune = listeCommunesExcel()
    for commune in listeCommune:
        cursor.execute(
            "INSERT INTO Communes(codeCommune, nomCommune, populationCommune, codeDepartement) VALUES (?, ?, ?, ?);",
            (commune.codeCommune, commune.nomCommune, commune.populationCommune, commune.codeDepartement)
        )
    connection.commit()


def ajoutDepartementsDansBaseDeDonnees():
    """Ajout des départements dans la base de données."""
    listeDepartement = listeDepartementsExcel()
    for departement in listeDepartement:
        cursor.execute(
            "INSERT INTO Departements(codeDepartement, nomDepartement, codeRegion) VALUES (?, ?, ?);",
            (departement.codeDepartement, departement.nomDepartment, departement.codeRegion)
        )
    connection.commit()


def ajoutRegionsDansBaseDeDonnees():
    """Ajout des régions dans la base de données."""
    listeRegion = listeRegionsExcel()
    for region in listeRegion:
        cursor.execute("INSERT INTO Regions(codeRegion, nomRegion) VALUES (?, ?);",
                       (region.codeRegion, region.nomRegion)
                       )
    connection.commit()


def afficherListeDepartementsExcel():
    """Récupération et affichage des départements depuis Excel."""

    listeDepartement = listeDepartementsExcel()
    for departement in listeDepartement:
        print("Nom = ", departement.nomDepartment, "\tCode = ", departement.codeDepartement, "\tCode de la région = ",
              departement.codeRegion)


def afficherListeCommunesExcel():
    """Récupération et affichage des communes depuis Excel."""

    listeCommune = listeCommunesExcel()
    for commune in listeCommune:
        print("Nom = ", commune.nomCommune, "\tCode = ", commune.codeCommune, "\tCode du département = ",
              commune.codeDepartement, "\tPopulation = ", commune.populationCommune)


def afficherListeRegionsExcel():
    """Récupération et affichage des régions depuis Excel."""

    listeRegion = listeRegionsExcel()
    for region in listeRegion:
        print("Nom = ", region.nomRegion, "\tCode = ", region.codeRegion)


def calculerPopulationDepartement(codeDepartement):
    """Retourne la population totale d'un département."""
    populationsDepartement = cursor.execute("SELECT populationCommune, codeCommune "
                                            "FROM Communes WHERE codeDepartement = ?;",
                                            (codeDepartement,)).fetchall()
    populationTotale = 0
    for population in populationsDepartement:
        # On convertit en entier (sans espace) les valeurs données pour la population
        if len(population[0]) > 3:
            populationTuple = population[0].split(" ")
            population = ""
            for elementTuple in populationTuple:
                population += elementTuple

            populationTotale += int(population)
        else:
            populationTotale += int(population[0])
    return populationTotale


def calculerPopulationRegion(codeRegion):
    """Calcule et affiche la population totale d'une région."""
    departementsDansRegion = cursor.execute("SELECT codeDepartement FROM Departements WHERE codeRegion = ?",
                                            (codeRegion,)).fetchall()
    populationRegion = 0
    for departement in departementsDansRegion:
        populationRegion += calculerPopulationDepartement(departement[0])

    return populationRegion


def afficherPopulationDeChaqueRegion():
    """Affiche la population de chaque région avec leur code et leur nom."""
    regions = cursor.execute("SELECT codeRegion, nomRegion FROM Regions;").fetchall()
    for region in regions:
        print("Pour ", region[0], " ", region[1], " :\tTotal = ", calculerPopulationRegion(region[0]))


def afficherPopulationDeChaqueDepartement():
    """Calcule et affiche la population de chaque département avec leur code et leur nom."""
    departements = cursor.execute("SELECT codeDepartement, nomDepartement "
                                  "FROM Departements;").fetchall()
    for departement in departements:
        print("Pour ", departement[0], " ", departement[1], " :\tTotal = ",
              calculerPopulationDepartement(departement[0]))


def afficherPopulationTotaleParDepartementEtParRegion_Q2():
    afficherPopulationDeChaqueDepartement()
    afficherPopulationDeChaqueRegion()


def trouverDepartementsAvecUnCommuneDeCeNom(nomDeLaCommune):
    # Sainte-Austreberthe est présent deux fois par exemple
    """Affiche la liste des départements où la commune se trouve. Cette fonction est utilisée en cas de doublon mais
    peut aussi être utilisée pour retourner le département où se trouve la commune si aucun doublon. """
    departements = cursor.execute("SELECT codeDepartement "
                                  "FROM Communes "
                                  "WHERE nomCommune = ?;", (nomDeLaCommune,)).fetchall()

    print("Pour la commune ", nomDeLaCommune, " on a : ")
    for departement in departements:
        print(departement[0])


def determinerDoublonDUneCommune(nomDeLaCommune):
    # Sainte-Colombe est présent treize fois par exemple
    """Cherche un nom de commune dans les départements et
    renvoie un booléen si doublon de la commune (si plus d'un département possède cette commune."""
    nombreDoublon = cursor.execute("SELECT COUNT(codeDepartement) "
                                   "FROM Communes "
                                   "WHERE nomCommune = ?;", (nomDeLaCommune,)).fetchall()

    if nombreDoublon[0][0] > 1:
        return True
    return False


def determinerCommunesPresentesDansPlusieursDepartements():
    """Determine les noms de communes présents dans plusieurs départements et donne la liste de ces départements."""
    communes = cursor.execute("SELECT nomCommune FROM Communes;").fetchall()

    for commune in communes:
        isDoublon = determinerDoublonDUneCommune(commune[0])
        if isDoublon:
            trouverDepartementsAvecUnCommuneDeCeNom(commune[0])


def supprimerTables():
    """ Suppression des tables si elles existent. """
#if len(cursor.execute(readTableCommunes).fetchall()) > 0:
    cursor.execute(deleteTableCommunes)
#if len(cursor.execute(readTableDepartements).fetchall()) > 0:
    cursor.execute(deleteTableDepartements)
#if len(cursor.execute(readTableRegions).fetchall()) > 0:
    cursor.execute(deleteTableRegions)


def creerTables():
    cursor.execute(createTableCommunes)
    cursor.execute(createTableDepartements)
    cursor.execute(createTableRegions)


def main():
    # Suppression et création des tables Communes, Regions et Departements si elles n'existent pas déjà
    supprimerTables()
    creerTables()

    # Récupération et affichage des communes depuis Excel
    # afficherListeCommunesExcel()

    # Récupération et affichage des départements depuis Excel
    # afficherListeDepartementsExcel()

    # Récupération et affichage des régions depuis Excel
    # afficherListeRegionsExcel()
    ajoutCommunesDansBaseDeDonnees()
    ajoutDepartementsDansBaseDeDonnees()
    ajoutRegionsDansBaseDeDonnees()


if __name__ == "__main__":
    main()
