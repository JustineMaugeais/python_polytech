# coding=utf8

import sqlite3

from TP5 import Q1

# Connexion à la base de données Pays
connection = Q1.connection

# Sera utile pour exécuter les requêtes
cursor = Q1.cursor


def calculerPopulationDepartement(codeDepartement):
    """Retourne la population totale d'un département."""
    populationsDepartement = cursor.execute("SELECT populationCommune, codeCommune "
                                            "FROM Communes WHERE codeDepartement = ?;",
                                            (codeDepartement,)).fetchall()
    populationTotale = 0
    for population in populationsDepartement:
        # On convertit en entier (sans espace) les valeurs données pour la population
        if len(population[0]) > 3:
            populationTuple = population[0].split(" ")
            population = ""
            for elementTuple in populationTuple:
                population += elementTuple

            populationTotale += int(population)
        else:
            populationTotale += int(population[0])
    return populationTotale


def calculerPopulationRegion(codeRegion):
    """Calcule et affiche la population totale d'une région."""
    departementsDansRegion = cursor.execute("SELECT codeDepartement FROM Departements WHERE codeRegion = ?",
                                            (codeRegion,)).fetchall()
    populationRegion = 0
    for departement in departementsDansRegion:
        populationRegion += calculerPopulationDepartement(departement[0])

    return populationRegion


def afficherPopulationDeChaqueRegion():
    """Affiche la population de chaque région avec leur code et leur nom."""
    regions = cursor.execute("SELECT codeRegion, nomRegion FROM Regions;").fetchall()
    for region in regions:
        print("Pour ", region[0], " ", region[1], " :\tTotal = ", calculerPopulationRegion(region[0]))


def afficherPopulationDeChaqueDepartement():
    """Calcule et affiche la population de chaque département avec leur code et leur nom."""
    departements = cursor.execute("SELECT codeDepartement, nomDepartement "
                                  "FROM Departements;").fetchall()
    for departement in departements:
        print("Pour ", departement[0], " ", departement[1], " :\tTotal = ",
              calculerPopulationDepartement(departement[0]))


def afficherPopulationTotaleParDepartementEtParRegion_Q2():
    afficherPopulationDeChaqueDepartement()
    afficherPopulationDeChaqueRegion()


def main():
    # Appel des fonctions de Q1 pour construire la base de données et évite la surhcarge dans ce code
    Q1.main()

    # Affichage de la population par département et région
    afficherPopulationTotaleParDepartementEtParRegion_Q2()


if __name__ == "__main__":
    main()
