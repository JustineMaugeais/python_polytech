# coding=utf8

import sqlite3

from TP5 import Q1

# Connexion à la base de données Pays
connection = Q1.connection

# Sera utile pour exécuter les requêtes
cursor = Q1.cursor


def trouverDepartementsAvecUnCommuneDeCeNom(nomDeLaCommune):
    """Affiche la liste des départements où la commune se trouve. Cette fonction est utilisée en cas de doublon mais
    peut aussi être utilisée pour retourner le département où se trouve la commune si aucun doublon. """
    # Sainte-Austreberthe est présent deux fois par exemple

    departements = cursor.execute("SELECT codeDepartement "
                                  "FROM Communes "
                                  "WHERE nomCommune = ?;", (nomDeLaCommune,)).fetchall()

    print("Pour la commune ", nomDeLaCommune, " on a : ")
    for departement in departements:
        print(departement[0])


def determinerDoublonDUneCommune(nomDeLaCommune):
    # Sainte-Colombe est présent treize fois par exemple
    """Cherche un nom de commune dans les départements et
    renvoie un booléen si doublon de la commune (si plus d'un département possède cette commune."""
    nombreDoublon = cursor.execute("SELECT COUNT(codeDepartement) "
                                   "FROM Communes "
                                   "WHERE nomCommune = ?;", (nomDeLaCommune,)).fetchall()

    if nombreDoublon[0][0] > 1:
        return True
    return False


def determinerCommunesPresentesDansPlusieursDepartements():
    """Determine les noms de communes présents dans plusieurs départements et donne la liste de ces départements."""
    communes = cursor.execute("SELECT nomCommune FROM Communes;").fetchall()

    for commune in communes:
        isDoublon = determinerDoublonDUneCommune(commune[0])
        if isDoublon:
            trouverDepartementsAvecUnCommuneDeCeNom(commune[0])


def main():
    # Appel des fonctions de Q1 pour construire la base de données et évite la surhcarge dans ce code
    Q1.main()

    # Détermine les doublons de la commune entrée en paramètre
    # determinerDoublonDUneCommune("Sainte-Austreberthe")

    # Détermine les doublons de chaque commune
    determinerCommunesPresentesDansPlusieursDepartements()


if __name__ == "__main__":
    main()
