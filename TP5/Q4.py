# coding=utf8

import xml.etree.cElementTree as ET

from TP5 import Q1

# Connexion à la base de données Pays
connection = Q1.connection

# Sera utile pour exécuter les requêtes
cursor = Q1.connection


def ecrireBaseDeDonneesDansXML():
    """
        Génère une sauvegarde de la base de données dans un fichier XML, correspondant au template global suivant:
        <pays>
            <regions>
                <region>
                    <id>...</id>
                    <nom>...</nom>
                    <departements>
                        <departement>
                            <id>...</id>
                            <nom>...</nom>
                            <communes>
                                <commune>
                                    <id>...</id>
                                    <nom>...</nom>
                                    <population>...</population>
                                </commune>
                            </communes>
                        </departement>
                    </departements>
                </region>
            </regions>
        </pays>
    """

    root = ET.Element('pays')
    """
    <pays>
        <regions>
        ...
        </regions>
    </pays>
    """
    regionsTree = ET.SubElement(root, "regions")

    regions = cursor.execute("SELECT * FROM Regions;").fetchall()
    for region in regions:
        """
        <region>
            <id>...</id>
            <nom>...</nom>
            <departements>
            ...
            </departements>
        </region>
        """
        regionTree = ET.SubElement(regionsTree, 'region')
        idRegion = ET.SubElement(regionTree, 'id')
        idRegion.text = region[0]
        nomRegion = ET.SubElement(regionTree, 'nom')
        nomRegion.text = region[1]

        departementsTree = ET.SubElement(regionTree, 'departements')

        departements = cursor.execute("SELECT * FROM Departements WHERE codeRegion = ?;", (region[0],)).fetchall()
        for departement in departements:
            """
            <departement>
                <id>...</id>
                <nom>...</nom>
                <communes>
                ...
                </communes>
            </departement>
            """
            departementTree = ET.SubElement(departementsTree, 'departement')
            idDepartement = ET.SubElement(departementTree, 'id')
            idDepartement.text = departement[0]
            nomDepartement = ET.SubElement(departementTree, 'nom')
            nomDepartement.text = departement[1]

            communesTree = ET.SubElement(departementTree, 'communes')

            communes = cursor.execute("SELECT * FROM Communes WHERE codeDepartement = ?;",
                                      (departement[0],)).fetchall()
            for commune in communes:
                """
                <commune>
                    <id></id>
                    <nom></nom>
                </commune>
                """
                communeTree = ET.SubElement(communesTree, 'commune')
                idCommune = ET.SubElement(communeTree, "id")
                idCommune.text = commune[0]
                nomCommune = ET.SubElement(communeTree, "nom")
                nomCommune.text = commune[1]
                populationCommune = ET.SubElement(communeTree, "population")
                populationCommune.text = commune[2]

    with open('DBToXML.xml', "wb") as fileXML:
        fileXML.write(ET.tostring(root))


def ecrireXMLDansBaseDeDonnees():

    tree = ET.parse('DBToXML.xml')
    root = tree.getroot()
    """
    <root>
        <regions>
        ...
        </regions>
    </root>
    """
    regionsTree = ET.Element.find(root, "regions")
    regions = regionsTree.findall('region')

    for region in regions:
        """
        <region>
            <id>...</id>
            <nom>...</nom>
            <departements>
            ...
            </departement>
        </region>
        """
        idRegion = str(region.find('id').text)
        nomRegion = str(region.find('nom').text)
        # print("id = ", idRegion, " nom = ", nomRegion)

        cursor.execute("INSERT INTO Regions(codeRegion, nomRegion) VALUES (?, ?);", (idRegion, nomRegion))

        """
        <departements>
        ...
        </departements>
        """
        departementsTree = region.find('departements')
        departements = departementsTree.findall('departement')
        for departement in departements:
            """
            <departement>
                <id>...</id>
                <nom>...</nom>
                <communes>
                ...
                </communes>
            </departement>
            """
            idDepartement = str(departement.find('id').text)
            nomDepartement = str(departement.find('nom').text)
            # print("idDep = ", idDepartement, " nomDep = ", nomDepartement)

            cursor.execute("INSERT INTO Departements(codeDepartement, nomDepartement, codeRegion) VALUES (?, ?, ?);",
                           (idDepartement, nomDepartement, idRegion))
            connection.commit()

            """
            <communes>
            ...
            </communes>
            """
            communesTree = departement.find('communes')

            communes = communesTree.findall("commune")

            for commune in communes:
                """
                <commune>
                    <id>...</id>
                    <nom>...</nom>
                    <population>...</population>
                </commune>
                """
                idCommune = str(commune.find("id").text)
                nomCommune = str(commune.find("nom").text)
                populationCommune = str(commune.find("population").text)
                # print("idCom = ", idCommune, " nomCom = ", nomCommune, " pop = ", populationCommune)
                cursor.execute("INSERT INTO Communes(codeCommune, nomCommune, populationCommune, codeDepartement) "
                               "VALUES (?, ?, ?, ?);", (idCommune, nomCommune, populationCommune, idDepartement))
                connection.commit()


def main():
    # Appel des fonctions de Q1 pour construire la base de données et évite la surhcarge dans ce code
    Q1.main()

    # Export de la base de données dans un fichier XML
    # ecrireBaseDeDonneesDansXML()

    # Suppression de la base de donnée et création des tables vides,
    # pour pouvoir n'avoir que les données récupérée du fichier XML.
    Q1.supprimerTables()
    Q1.creerTables()

    # Import du fichier XML dans la base de données
    ecrireXMLDansBaseDeDonnees()


if __name__ == "__main__":
    main()
