import numpy as np


def creerTableauDimension3AvecNombresAleatoires():
    """ Cree un tableau de dimension 3 de cette forme :
    [[[[], []], [[], []], [[], []]], [[[], []], [[], []], [[], []]], [[[], []], [[], []], [[], []]], [[[], []], [[], []], [[], []]]]
    """
    tableauDimension3 = np.random.rand(4, 3, 2)
    print("Tableau genere :\n"
          + str(tableauDimension3)
          + "\n\nAttributs du tableau :\nDimension : " + str(tableauDimension3.ndim)
          + "\nCorps : " + str(tableauDimension3.shape)
          + "\nTaille : " + str(tableauDimension3.size)
          + "\nType : " + str(tableauDimension3.dtype)
          + "\nTaille des items : " + str(tableauDimension3.itemsize)
          + "\nDonnee : " + str(tableauDimension3.data))
    return tableauDimension3


def main():
    creerTableauDimension3AvecNombresAleatoires()


if __name__ == "__main__":
    main()
