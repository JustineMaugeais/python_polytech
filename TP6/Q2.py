import numpy as np


def creerMatrices3x3(de, a):
    return np.random.randint(de, a, size=(3, 3))


def produitIndexMatrice(mat1, mat2):
    """ Retourne une matrice de produits des indices a la meme position.
    On a produit(i, j) = mat1(i, j) * mat2(i, j)."""
    return mat1 * mat2


def produitMatrice(mat1, mat2):
    """ Retourne une matrice resultant du produit matriciel entre les deux matrices.
    On a produit = mat1 * mat2."""
    return mat1.dot(mat2)


def transposerMatrice(mat):
    """ Transpose une matrice."""
    arrayMat = np.array(mat)
    return arrayMat.transpose()


def main():
    matrice1 = creerMatrices3x3(0, 8)
    matrice2 = creerMatrices3x3(2, 10)
    produitIndex = produitIndexMatrice(matrice1, matrice2)
    produit = produitMatrice(matrice1, matrice2)
    transposeeMatrice1 = transposerMatrice(matrice1)

    print("Matrice 1 :\n" + str(matrice1)
          + "\n\nMatrice 2 :\n" + str(matrice2)
          + "\n\nProduit des deux :\n" + str(produit)
          + "\n\nProduit des index des deux :\n" + str(produitIndex)
          + "\n\nTransposee de la matrice 1 :\n" + str(transposeeMatrice1))


if __name__ == "__main__":
    main()
