import numpy as np

from TP6 import Q2


def calculerDeterminantMatrice(mat):
    """ Calcul du determinant d\'une matrice."""
    return np.linalg.det(mat)


def calculerInverseMatrice(mat):
    """Calcule l'inverse d\'une matrice."""
    return np.linalg.inv(mat)


def resoudreSystemeLineaire(a, b):
    """Resolution du systeme d\'equation de la forme AX = B."""
    A = np.array(a)
    B = np.array(b)
    X = np.linalg.solve(A, B)

    return X


def calculerValeursPropres(mat):
    """ Calcule les valeurs et vecteurs propres d\'une matrice."""
    return np.linalg.eig(mat)


def main():
    # Appel de Q2 pour generer une matrice 3x3
    # Membre de gauche dans le systeme
    matrice1 = Q2.creerMatrices3x3(0, 10)
    inverseMatrice1 = calculerInverseMatrice(matrice1)
    determinant = calculerDeterminantMatrice(matrice1)

    # Creation d'une matrice 3x1 pour la resolution du systeme
    # Membre de droite dans le systeme
    matrice2 = np.random.randint(1, 5, size=(3, 1))

    resolutionSysteme = resoudreSystemeLineaire(matrice1, matrice2)

    valeursPropres = calculerValeursPropres(matrice1)

    print("Matrice :\n" + str(matrice1)
          + "\n\nDeterminant : " + str(determinant)
          + "\n\nInverse :\n" + str(inverseMatrice1)
          + "\n\nResolution systeme :\n" + str(resolutionSysteme)
          + "\n\nValeurs et vecteurs propres de la matrice :\n" + str(valeursPropres))


if __name__ == "__main__":
    main()
