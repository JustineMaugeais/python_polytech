import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit


def fonction(x, a, b, c):
    """Definit la fonction a approcher."""
    return a * np.exp(-b * x) + c


def creerNuageDePointsBruites():
    """ Creation d\'un jeu de donnees
    Appelle la fonction definie dans fonction()
    Genere un nombre de points bruites de facon aleatoire. """
    xdata = np.linspace(0, 4, 50)

    # Fonction definie
    y = fonction(xdata, 2.5, 1.3, 0.5)

    # Creation du rang du bruit
    rng = np.random.default_rng()

    # Creation de la fonction bruitee
    y_noise = 0.2 * rng.normal(size=xdata.size)

    # Point genere depuis la fonction y avec le bruit
    ydata = y + y_noise

    # Ajout du nuage de points crees dans la graphique en bleu
    plt.plot(xdata, ydata, 'b.', label='data')

    # Construction de la courbe d'approche en fonction des parametres a et b
    popt, pcov = curve_fit(fonction, xdata, ydata)
    plt.plot(xdata, fonction(xdata, *popt), 'r-',
             label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))


def main():
    creerNuageDePointsBruites()
    plt.show()


if __name__ == "__main__":
    main()
