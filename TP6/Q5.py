from PIL import Image


def main():
    # Ouverture de l'image
    tuxImage = Image.open("tux.jpeg")

    # Affichage de l'image
    tuxImage.show()

    # Redimensionnement de l'image
    tuxImagePlusPetite = tuxImage.resize((20, 30))

    # Affichage de l'image redimensionnee
    tuxImagePlusPetite.show()


if __name__ == "__main__":
    main()
