#!/usr/bin/python3

import cgi

form = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")

html = """<!DOCTYPE html>
<head>
 <title>Mon programme Justine</title>
</head>
<body>
 <form action="/index.py" method="post">
 <input type="text" name="name" value="Nom" />
 <input type="text" name="password" value="Mot de passe" />
 <input type="submit" name="send" value="Envoyer information au serveur">
 </form> 
</body>
</html>
"""
print(html)

# Recuperation du nom
nom = form.getvalue("name")

# Recuperation du mot de passe
motdepasse = form.getvalue("password")

# Affichage dans la page pour bien montrer que les valeurs sont reecuperees
print("<br/>Nom : ", nom, "<br/>Mot de passe : ", motdepasse)

