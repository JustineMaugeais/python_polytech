# coding=utf8
# !/usr/bin/python3

import cgi

from TP7.Q4.utilisateur import Utilisateur, verifierAcces

form = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")

html = """<!DOCTYPE html>
<head>
 <title>Mon programme Justine</title>
</head>
<body>
 <form action="/index.py" method="post">
 <input type="text" name="name" value="Nom" />
 <input type="text" name="password" value="Mot de passe" />
 <input type="submit" name="send" value="Envoyer information au serveur">
 </form> 
</body>
</html>
"""
print(html)

# Recuperation du nom
nom = form.getvalue("name")

# Recuperation du mot de passe
motdepasse = form.getvalue("password")

# Creation utilisateur
utilisateur = Utilisateur(nom, motdepasse)

# Affichage dans la page pour bien montrer que les valeurs sont reecuperees

# Vérifie si les informations de l'utilisateur sont correctes.
if verifierAcces(utilisateur):

    # Infos OK donc affichage des données de l'utilisateur
    print("<br/>Voici vos informations personnelles :"
          "<br/>Nom : ", nom,
          "<br/>Mot de passe : ", motdepasse)

else:
    # Infos KO donc affichage d'un message d'erreur
    print("<br/>Vous ne disposez pas des droits pour acceder a ces informations.")
