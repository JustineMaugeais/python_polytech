# coding=utf8
import mysql.connector as con


"""Connnexion à la base de données existante."""
connection = con.connect(
    # On peut changer l'adresse IP pour se connecter à distance à la base
    # Il faudra juste gérer les problèmes de proxy
    host="localhost",
    database="utilisateur",
    user="root",
    password="")

cursor = connection.cursor()

readTableUtilisateurs = "SELECT * FROM Utilisateurs;"


class Utilisateur:
    """ Classe utilisateur permettant de stocker mot de passe et
    nom d\' un utilisateur lors de sa saisie."""

    def __init__(self, nom, motdepasse):
        self.nom = nom
        self.motdepasse = motdepasse


def verifierAcces(utilisateur):
    """Vérifie la connexion utilisateur lors de la saisie.
    Si son nom ou son mot de passe n'est pas bon,
    il ne peut pas accéder aux informations, donc faux.
    Vrai sinon."""
    cursor.execute(readTableUtilisateurs)
    utilisateurs = cursor.fetchall()
    for utilisateurDansBD in utilisateurs:
        if (utilisateurDansBD[0] == utilisateur.nom) and utilisateurDansBD[1] == utilisateur.motdepasse:
            return True

    return False


def main():
    """
    Connexion OK :
       nom = utilisateur1
       mdp = mdp1
    """
    pass


if __name__ == "__main__":
    main()
